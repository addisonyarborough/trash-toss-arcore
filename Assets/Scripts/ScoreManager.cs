﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    // The audio source that we will play our sounds from
    [SerializeField]
    AudioSource audioSource;

    // The sound played when we score a goal
    [SerializeField]
    AudioClip scoreSound;

    // This is called from Trashcan.cs when a ball enters its trigger collider
    public void OnGoalScored()
    {
        // Play the goal score sound
        audioSource.PlayOneShot(scoreSound, 0.25f);
    }
}
