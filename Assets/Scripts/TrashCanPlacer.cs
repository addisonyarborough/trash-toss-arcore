﻿using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.Rendering;
using GoogleARCore.HelloAR;

public class TrashCanPlacer : MonoBehaviour
{
    // Our score manager - we will pass this reference to any new trash cans that we create
    [SerializeField]
    ScoreManager scoreManager;

    // Our ball thrower - we will initialize this script as soon as the trash can is placed so that we can being throwing
    [SerializeField]
    BallThrower ballThrower;

    /// A prefab for tracking and visualizing detected planes.
    [SerializeField]
    GameObject trackedPlanePrefab;

    /// A model to place when a raycast from a user touch hits a plane.
    [SerializeField]
    GameObject trashCanPrefab;

    /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
    [SerializeField]
    GameObject searchingForPlaneUI;

    /// A list to hold new planes ARCore began tracking in the current frame. This object is used across
    /// the application to avoid per-frame allocations.
    List<TrackedPlane> newPlanes = new List<TrackedPlane>();

    /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
    /// the application to avoid per-frame allocations.
    List<TrackedPlane> allPlanes = new List<TrackedPlane>();

    // Have we already placed our trashcan?
    [SerializeField]
    bool isTrashCanPlaced;

    void Awake()
    {
        // Make sure our screen doesn't dim or go to sleep while we're playing the game
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void Update()
    {
        // If the user presses the back key, quit the game
        CheckForQuit();
        // Visualize the mesh that we're creating by scanning the environment
        VisualizePlanes();
        // Show some UI if the user is still searching for planes
        ShowUI();
        // If the user taps on the screen, place the trashcan at that point
        CheckForPlaceTrashCan();
    }

    void VisualizePlanes()
    {
        // Get the planes in this current frame
        Frame.GetPlanes(newPlanes, TrackableQueryFilter.New);

        // For each frame, make a prefab that helps visualize where it is
        for (int i = 0; i < newPlanes.Count; i++)
        {
            // Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
            // the origin with an identity rotation since the mesh for our prefab is updated in Unity World
            // coordinates.
            GameObject planeObject = Instantiate(trackedPlanePrefab, Vector3.zero, Quaternion.identity, transform);

            // Add a visulizer component to the instantiated plane prefab
            planeObject.GetComponent<TrackedPlaneVisualizer>().Initialize(newPlanes[i]);

            // Add a collider so that our ball will hit it if we miss the trash can
            planeObject.AddComponent<MeshCollider>();
        }
    }

    void ShowUI()
    {
        // Get all planes in this current frame
        Frame.GetPlanes(allPlanes);
        bool showSearchingUI = true;

        // For each plane - if any of them are in the tracking state - disable the UI
        for (int i = 0; i < allPlanes.Count; i++)
        {
            if (allPlanes[i].TrackingState == TrackingState.Tracking)
            {
                showSearchingUI = false;
                break;
            }
        }

        // Set the searching UI active or inactive depending on whether or not we've finished searching
        searchingForPlaneUI.SetActive(showSearchingUI);
    }

    void CheckForPlaceTrashCan()
    {
        // If the user taps on the screen and we haven't yet placed a trash can...
        if (Input.GetMouseButtonDown(0) && !isTrashCanPlaced)
        {
            // If we're running this in the editor..
            if (Application.isEditor)
            {
                // Get a point 3 meters in front of the camera to place the trashcan at
                var infrontOfCamera = Camera.main.transform.TransformPoint(0, -1, 3);

                // Create a new trashcan by cloning our prefab - and place it at the hit position
                CreateTrashCan(infrontOfCamera);
            }
            else
            {
                // If we're running this on a mobile device...

                // Raycast against the location the player touched to search for planes.
                TrackableHit hit;
                TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

                // Get the user's pointer position on the screen
                var pointerPosition = Input.mousePosition;

                if (Session.Raycast(pointerPosition.x, pointerPosition.y, raycastFilter, out hit))
                {
                    // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    // Create a new trash can and set its position to the hit position of our raycast
                    var trashCan = CreateTrashCan(hit.Pose.position);

                    // Make trash can model a child of the anchor.
                    trashCan.transform.SetParent(anchor.transform);
                }
                else
                {
                    // If we didn't hit anything, return so that we don't initialize our ball throwe below
                    return;
                }
            }

            // Initialize the paper thrower
            ballThrower.Init();
        }
    }

    GameObject CreateTrashCan(Vector3 position)
    {
        // Create a new trashcan by cloning our prefab - and place it at the hit position
        var trashCan = Instantiate(trashCanPrefab, position, Quaternion.identity);

        // Add a Trashcan.cs script to our new trash can
        var trashCanScript = trashCan.AddComponent<TrashCan>();

        // Initialize the trashcan, and pass in our score manager so that it has a reference
        trashCanScript.Init(scoreManager);

        // Change the bool so we'll know we've already placed the trashcan
        isTrashCanPlaced = true;

        // Pass back the trash can to the caller of this method
        return trashCan;
    }

    void CheckForQuit()
    {
        // If the user taps the back button, exit the game
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
