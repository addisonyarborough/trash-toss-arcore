﻿using UnityEngine;

public class TrashCan : MonoBehaviour
{
    // A reference to the score manager - supplied during the Init method
    ScoreManager scoreManager;

    bool isBallInTrashCan;

    // Called via TrashcanPlacer.cs as soon as the trashcan is placed
    public void Init(ScoreManager scoreManager)
    {
        // Get an initial reference to the score manager
        this.scoreManager = scoreManager;
    }

    void OnTriggerEnter(Collider collider)
    {
        // If a ball is already in the trash can, return. This prevents multiple collisions
        if (isBallInTrashCan)
            return;

        isBallInTrashCan = true;

        // Check to make sure that the thing that just collided with us is tagged "Ball"
        if (collider.tag != "Ball")
            return;

        // Let the score manager know that we've scored a goal
        scoreManager.OnGoalScored();
    }

    void OnTriggerExit()
    {
        // Once the ball exits the collider, make sure we set the bool back to false
        isBallInTrashCan = false;
    }

}
