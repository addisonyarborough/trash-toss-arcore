﻿using UnityEngine;
using UnityEngine.UI;

public class BallThrower : MonoBehaviour
{
    // The gameObject of our ball
    [SerializeField]
    GameObject ball;

    // The rigidbody component of our ball
    [SerializeField]
    Rigidbody ballRigidbody;

    // The element that we can drag downward on to set more or less power on the power bar
    [SerializeField]
    RectTransform sliderHandle;

    // The scroll rect that manages the slider
    [SerializeField]
    ScrollRect powerBarScrollRect;

    // The fill bar that displays our current power
    [SerializeField]
    Image powerBarFill;

    // The sound we will play when the sliderHandle is released
    [SerializeField]
    AudioClip throwSound;

    [SerializeField]
    // Is the handle currently being dragged?
    bool isDragging;

    // The number to multiply the power that we calculate by
    [Tooltip("Increase or decrease this to change the actual force that the ball receives when tossed relative to the power bar.")]
    [SerializeField]
    [Range(0.01f, 10.0f)]
    float powerMultiplyer;

    // Called from TrashcanPlacer as soon as the trash can is placed
    public void Init()
    {
        // Since the ball starts inactive, set the ball active once we've placed the trash can
        ball.gameObject.SetActive(true);
    }

    // Called from the Scroll Rect's Event Trigger component (On Pointer Down)
    public void OnHandleGrab()
    {
        // We've started a drag on the power handle
        isDragging = true;

        // Reset the ball to its screen position
        ReturnBallToStartPosition();
    }

    void Update()
    {
        // If we are dragging the power handle and we got a pointer up event..
        if (isDragging && Input.GetMouseButtonUp(0))
        {
            // Release the power handle
            ReleaseHandle();
        }
    }

    void ReleaseHandle()
    {
        // Let this script know that we're no longer dragging
        isDragging = false;

        // If the slider has been dragged up instead of down, ignore the rest of this method
        if (sliderHandle.anchoredPosition.y > 0)
            return;

        // Calculate the velocity to launch the ball at based on how far the user has dragged the slider
        var power = Mathf.Abs(sliderHandle.anchoredPosition.y) * powerMultiplyer;

        // If our power is less than 5, the user probably didn't mean to actually toss the ball - so return
        if (power < 5)
            return;

        // Turn the gravity on for this object
        ballRigidbody.useGravity = true;

        // Turn off the kinematic flag for this object
        ballRigidbody.isKinematic = false;

        // Un-parent our ball from the camera so that its position isn't influenced by the camera's movement
        ball.transform.SetParent(null);

        // Get the camera's forward vector to use for the z (forward pointing) force
        var camForward = Camera.main.transform.forward;

        // Assign both the up and forward parts of our force with the power we received from the slider
        var force = new Vector3(0, power, power);

        // Convert our force from world space to (the main camera's) local space
        force = Camera.main.transform.TransformDirection(force);

        // Apply velocity to the ball
        ballRigidbody.AddForce(force);

        // Play the throwing sound on the ball itself
        ball.GetComponent<AudioSource>().PlayOneShot(throwSound);
    }

    void ReturnBallToStartPosition()
    {
        // Re-parent our ball to the main camera so that it stays locked until we throw it again
        ball.transform.SetParent(Camera.main.transform);

        // Move the ball back to its starting position
        ball.transform.localPosition = Vector3.forward * 0.5f;

        // Reset the rigidbody's velocity to zero (stop it from moving)
        ballRigidbody.velocity = Vector3.zero;

        // Let the ball know to stop responding to gravity
        ballRigidbody.useGravity = false;

        // Let the ball know that we'd like to control it using code instead of physics
        ballRigidbody.isKinematic = true;
    }

    // This is called via the ScrollRect component by selecting this method in the the OnValueChanged UnityEvent field
    public void UpdatePowerBar()
    {
        // If the power bar was dragged upwards instead of downwards, return
        if (sliderHandle.anchoredPosition.y > 0)
            return;

        // Get the absolute value of our power bar's handle's position since we dragged it down/negatively
        var powerBarPosition = Mathf.Abs(sliderHandle.anchoredPosition.y);

        // This value comes from the anchored Y position of the handle if it is dragged all the way down
        var maxPowerBarValue = 123.0f;

        // Normalize this value - then multiply it by 2.5 so that the power bar will fill up a bit faster
        var normalizedPowerBarValue = (powerBarPosition / maxPowerBarValue) * 2.5f;

        // Apply the normalized value to our bar fill amount
        powerBarFill.fillAmount = normalizedPowerBarValue;
    }
}
