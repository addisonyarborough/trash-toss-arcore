# README #

Welcome to Trash Toss for ARCore!

### What is this repository for? ###

* This repository is for YouVisit's AR game 'Trash Toss.' There are two separate repositories for this game. One is for usage with ARKit, the other with ARCore.

### How do I get set up? ###

* To get started, visit this link and follow all instructions: https://developers.google.com/ar/develop/unity/getting-started
* Once you have completed all of the instructions in the above link, import the ARCore Unity package into your project.
* You may now navigate to the scenes folder and double-click "TrashTossARCore".

### Who do I talk to? ###

* Questions or comments? You may email the admin of this repo - addison.yarborough@youvisit.com